package info.atende.webutil.jsf.converters.joda.datetime;

import info.atende.webutil.jsf.converters.joda.Common;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * Conversor para DateTime
 * Criado por giovanni.
 * Data: 28/04/12
 * Hora: 18:21
 */
public class DateTimeConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String pattern = Common.getPattern(component);
        DateTime dateTime = null;
        try{
            dateTime = DateTimeFormat.forPattern(pattern).parseDateTime(value);
        }catch (Exception ex){
            throw new ConverterException(ex);
        }
        return dateTime;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String pattern = Common.getPattern(component);
        String dateTime;
        try{
            dateTime = DateTimeFormat.forPattern(pattern).print((DateTime) value);
        }catch (Exception ex){
            throw  new ConverterException(ex);
        }
        return dateTime;
    }
}
