/*
 * Copyright (c) 2012. Atende Tecnologia da Informação.
 * Este software é propriedade de Atende Tecnologia da Informação e está protegido por
 * leis internacionais de direitos autorais.
 */

package info.atende.webutil.jsf.converters.joda.datetime;

import org.joda.time.DateTime;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Facade para DateTime implicito
 * Criado por giovanni.
 * Data: 30/04/12
 * Hora: 11:04
 */
@FacesConverter(forClass = DateTime.class)
public class DateTimeConverterForClass implements Converter {
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
       return new DateTimeConverter().getAsObject(context, component, value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return new DateTimeConverter().getAsString(context, component, value);
    }
}
