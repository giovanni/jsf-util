For use the converters put the lines bellow in your faces-config.xml
<converter>
        <converter-for-class>org.joda.time.LocalDateTime</converter-for-class>
        <converter-class>info.atende.webutil.jsf.converters.joda.localdatetime.LocalDateTimeConverterForClass</converter-class>
    </converter>
    <converter>
        <converter-id>jodaLocalDateTime</converter-id>
        <converter-class>info.atende.webutil.jsf.converters.joda.localdatetime.LocalDateTimeConverterWithId</converter-class>
    </converter>
    <converter>
        <converter-for-class>org.joda.time.DateTime</converter-for-class>
        <converter-class>info.atende.webutil.jsf.converters.joda.datetime.DateTimeConverterForClass</converter-class>
    </converter>
    <converter>
        <converter-id>jodaDateTime</converter-id>
        <converter-class>info.atende.webutil.jsf.converters.joda.datetime.DateTimeConverterWithId</converter-class>
</converter>